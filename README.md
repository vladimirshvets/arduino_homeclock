# Home Clock #

Home clock with temperature and light sensor based on Arduino Pro Micro (ATMega32u4).
WS2812b LEDs used to emulate 8-segment 4-digit display.


### Summary ###

* Author: Vladimir Shvets <uladzimir.shvets@gmail.com>
* Version: 0.0.1
* Release date: Feb 07, 2020
* [Repo on Bitbucket](https://bitbucket.org/vladimirshvets/arduino_homeclock)


### Required components ###

* Platform: 
	Arduino Pro Micro
* Temperature sensor: 
	Dallas DS18b20
* Light sensor (photocell): 
	GL5528
* Power supply: 
	5V 50W (10A)
* Indication: 
	WS2812b LEDs (100 LEDs/m, 30W/m)
* Resistors: 
	* 4.7k for thermosensor
	* 10k for photocell
* Keyboard: ...



### How to set up? ###

* IDE: Arduino 1.8.11


### Arduino Leonardo ###

* Pin 2 - Clock SDA
* Pin 3 - Clock SCL
* Pin 8 - One wire bus (thermosensor)
* Analog pin A0 - photocell