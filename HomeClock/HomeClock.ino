#include <DallasTemperature.h>
#include <DS1307RTC.h>
#include <FastLED.h>
#include <OneWire.h>
#include <Time.h>
#include <Wire.h>


/* Light sensor analog pin A0 */
#define PIN_PHOTOCELL           0
/**
 * One wire bus pin
 * Temperature sensor
 */
#define PIN_ONE_WIRE            14
/* Led matrix data pin */
#define PIN_DISPLAY             10
/* LEONARDO LED PIN */
#define PIN_LEO_LED             13



#define PIN_L_DT                0 // interrupt
#define PIN_L_SW                1 // interrupt
#define PIN_L_CLK               6 // non-int (works on L_DT)
#define PIN_R_CLK               7 // interrupt
#define PIN_R_DT                8 // non-int (works on R_CLK)
#define PIN_R_SW                9 // non-int


#define LED_AMOUNT              119
/* Digit has 4x7 size */
#define DIGIT_WIDTH             4
#define DIGIT_HEIGHT            7

#define SEPARATOR_OFF           0
#define SEPARATOR_DOT           1
#define SEPARATOR_COLON         2



const byte MODE_NO_TIME       = 100;
const byte MODE_DATE          = 101;
const byte MODE_TIME          = 102;
const byte MODE_TEMPERATURE   = 103;
const byte MODE_BRIGHTNESS    = 104; 


const byte MODE_SET_YEAR      = 110;
const byte MODE_SET_MONTH     = 111;
const byte MODE_SET_DAY       = 112;
const byte MODE_SET_HOUR      = 113;
const byte MODE_SET_MINUTE    = 114;
const byte MODE_SET_SECOND    = 115;

/**
 * The sequence of LEDs to be switched on to display digits 0-9
 * Each digit has dimensions 4x7 with no spaces
 * Digit 10 is empty digit
 * Digit 11 is degree char
 * Digit 12 is celsium char
 */
const byte digits47[13][28] = {
  { 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 }, 
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 }, 
  { 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1 }, 
  { 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 }, 
  { 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1 }, 
  { 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1 }, 
  { 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1 }, 
  { 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 }, 
  { 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 }, 
  { 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1 },
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
  { 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0 }, // degree
  { 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1 }, // celsium
};

const byte separator7[3][7] = {
  { 0, 0, 0, 0, 0, 0, 0 },    // off
  { 1, 0, 0, 0, 0, 0, 0 },    // dot
  { 0, 1, 0, 0, 0, 1, 0 }     // colon
};


/**
 * vars
 */
tmElements_t datetime, modifyDatetime;

CRGB strip[LED_AMOUNT];

OneWire oneWire(PIN_ONE_WIRE);

DallasTemperature thermosensor(&oneWire);

float temperature;

byte mode;

int lightValue;

boolean tickColon = false;
unsigned long tickTiming;

byte rswPressedFlag = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("Home Clock v.0.0.1");
  Serial.println("by Vladimir Shvets <uladzimir.shvets@gmail.com>");
  Serial.println("Feb 07, 2020");

  FastLED.addLeds<WS2812B, PIN_DISPLAY, GRB>(strip, LED_AMOUNT);
  thermosensor.setResolution(12);
  pinMode(PIN_LEO_LED, OUTPUT);

  //Устанавливаем нужное время в формате: Часы, минуты, секунды, день, месяц, год
  //setTime(16, 11, 40, 20, 4, 2020);
  //Применяем значение:
  //RTC.set(now());

  attachInterrupt(digitalPinToInterrupt(PIN_L_SW), leftClickInt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_L_DT), leftRotateInt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(PIN_R_CLK), rightRotateInt, CHANGE);
  mode = MODE_TIME;
}

void leftClickInt() {
  byte lsw = digitalRead(PIN_L_SW);
  if (lsw == 1) {
    printText("L BUTTON");
    switch (mode) {
      case MODE_TIME:
        mode = MODE_SET_YEAR;
        modifyDatetime = datetime;
        break;
      case MODE_SET_YEAR:
        mode = MODE_TIME;
        break;
      case MODE_SET_MONTH:
        mode = MODE_SET_YEAR;
        break;
      case MODE_SET_DAY:
        mode = MODE_SET_MONTH;
        break;
      case MODE_SET_HOUR:
        mode = MODE_SET_DAY;
        break;
      case MODE_SET_MINUTE:
        mode = MODE_SET_HOUR;
        break;
      case MODE_SET_SECOND:
        mode = MODE_SET_MINUTE;
        break;
    }
  }
}

void rightClick() {
  byte rsw = digitalRead(PIN_R_SW);
  if (rsw == 1) {
    printText("R BUTTON");
    printText(String(rswPressedFlag));
    if (rswPressedFlag == 0) {
      rswPressedFlag == 1;
      switch (mode) {
        case MODE_SET_YEAR:
          mode = MODE_SET_MONTH;
          break;
        case MODE_SET_MONTH:
          mode = MODE_SET_DAY;
          break;
        case MODE_SET_DAY:
          mode = MODE_SET_HOUR;
          break;
        case MODE_SET_HOUR:
          mode = MODE_SET_MINUTE;
          break;
        case MODE_SET_MINUTE:
          mode = MODE_SET_SECOND;
          break;
        case MODE_SET_SECOND:
          setTime(modifyDatetime.Hour, modifyDatetime.Minute, modifyDatetime.Second, modifyDatetime.Day, modifyDatetime.Month, tmYearToCalendar(modifyDatetime.Year));
          RTC.set(now());
          mode = MODE_TIME;
          break;
      }
    }
  } else {
    rswPressedFlag = 0;
  }
}

void leftRotateInt() {
  if (digitalRead(PIN_L_CLK) == digitalRead(PIN_L_DT)) {
    printText("L ROTATE RIGHT");
  } else {
    printText("L ROTATE LEFT");
  }
}

void rightRotateInt() {
  byte c = digitalRead(PIN_R_CLK);
  byte d = digitalRead(PIN_R_DT);
  switch (mode) {
    
    case MODE_SET_YEAR:
      if (c == d) {
        modifyDatetime.Year--;
      } else {
        modifyDatetime.Year++;
      }
      printText("mdt.Year: " + String(modifyDatetime.Year));
      break;
      
    case MODE_SET_MONTH:
      if (c == d) {
        modifyDatetime.Month--;
      } else {
        modifyDatetime.Month++;
      }
      printText("mdt.Month: " + String(modifyDatetime.Month));
      break;

    case MODE_SET_DAY:
      if (c == d) {
        modifyDatetime.Day--;
      } else {
        modifyDatetime.Day++;
      }
      printText("mdt.Day: " + String(modifyDatetime.Day));
      break;

    case MODE_SET_HOUR:
      if (c == d) {
        modifyDatetime.Hour--;
      } else {
        modifyDatetime.Hour++;
      }
      printText("mdt.Hour: " + String(modifyDatetime.Hour));
      break;

    case MODE_SET_MINUTE:
      if (c == d) {
        modifyDatetime.Minute--;
      } else {
        modifyDatetime.Minute++;
      }
      printText("mdt.Minute: " + String(modifyDatetime.Minute));
      break;

    case MODE_SET_SECOND:
      if (c == d) {
        modifyDatetime.Second--;
      } else {
        modifyDatetime.Second++;
      }
      printText("mdt.Second: " + String(modifyDatetime.Second));
      break;
      
    default:
      if (c == d) {
        printText("R ROTATE LEFT");
        mode--;
        if (mode < MODE_DATE) {
          mode = MODE_BRIGHTNESS;
        }
      } else {
        printText("R ROTATE RIGHT");
        mode++;
        if (mode > MODE_BRIGHTNESS) {
          mode = MODE_DATE;
        }
      }
  }
}


void loop() {
  processMode();
  delay(50);

  if (mode == MODE_TIME || mode == MODE_NO_TIME) {
    /* Read data from DS3231 */
    if (RTC.read(datetime)) {
      mode = MODE_TIME;
    //    if (datetime.Second >= 20 && datetime.Second < 25  && (datetime.Minute == 15 || datetime.Minute == 45)) {
    //      mode = MODE_TEMPERATURE;
    //    } else if (datetime.Second >= 40 && datetime.Second < 45 && datetime.Hour >= 8 && datetime.Hour < 22 && datetime.Minute % 20 == 0) {
    //      mode = MODE_DATE; 
    //    } else if (datetime.Second >= 50 && datetime.Second < 55  && datetime.Hour >= 8 && datetime.Hour < 23) {
    //      mode = MODE_BRIGHTNESS;
    //    }
    } else {
      mode = MODE_NO_TIME;
    }
  }
  
  

  if (millis() - tickTiming >= 500) {
      tickColon = !tickColon;
      tickTiming = millis();
    }

  lightValue = getBrightness();
  //printText("Photocell: " + String(lightValue));
  //lightValue = lightValue / 5;
  if (lightValue < 1) lightValue = 1;
  if (lightValue > 50) lightValue = 50;

  
}


void printText(String text) {
  Serial.println(text);
}


void processMode() {
  switch (mode) {
    case MODE_SET_YEAR:
      //printText("Set year: " + String(modifyDatetime.Year));
      rightClick();
      break;

    case MODE_SET_MONTH:
      //printText("Set month: " + String(modifyDatetime.Month));
      rightClick();
      break;

    case MODE_SET_DAY:
      //printText("Set day: " + String(modifyDatetime.Day));
      rightClick();
      break;

      case MODE_SET_HOUR:
      //printText("Set hour: " + String(modifyDatetime.Hour));
      rightClick();
      break;

    case MODE_SET_MINUTE:
      //printText("Set minute: " + String(modifyDatetime.Minute));
      rightClick();
      break;

    case MODE_SET_SECOND:
      //printText("Set second: " + String(modifyDatetime.Second));
      rightClick();
      break;

    
    case MODE_TIME:
      displayTime();
      printText(String(datetime.Hour) + ":" + String(datetime.Minute) + ":" + String(datetime.Second));
      printText(String(datetime.Day) + "." + String(datetime.Month) + "." + String(tmYearToCalendar(datetime.Year)));
      break;

    case MODE_DATE:
      displayDate();
      //printText(String(datetime.Day) + "." + String(datetime.Month) + "." + String(tmYearToCalendar(datetime.Year)));
      break;

    case MODE_BRIGHTNESS:
      //printText("Brightness: " + lightValue);
      displayBrightness();
      break;

    case MODE_TEMPERATURE:
      String t = getTemperature(); 
      //printText(t);
      displayTemperature();
      break;

    case MODE_NO_TIME:
    default:
      printText("No time set");
      break;
  }
  FastLED.show();
}


/**
 * Get temperature from sensor
 */
String getTemperature() {
  thermosensor.requestTemperatures();
  temperature = thermosensor.getTempCByIndex(0);
  return String(temperature) + "°C";
}

int getBrightness() {
  return map(analogRead(PIN_PHOTOCELL), 0, 1023, 0, 255);
}


void displayTime() {
  CRGB color = CRGB(0, lightValue, 0);
  if (datetime.Hour >= 22 || datetime.Hour <= 6) {
    color = CRGB(0, 0, lightValue);
  }
  setDigit(1, datetime.Hour / 10, color);
  setDigit(2, datetime.Hour % 10, color);
  setDigit(3, datetime.Minute / 10, color);
  setDigit(4, datetime.Minute % 10, color);
  //setDigit(3, datetime.Second / 10, color);
  //setDigit(4, datetime.Second % 10, color);
  if (tickColon) {
    setSeparator(SEPARATOR_COLON, color);
  } else {
    setSeparator(SEPARATOR_OFF, color);
  }
}


void displayDate() {
  CRGB color = CRGB(lightValue, 0, lightValue);
  setDigit(1, datetime.Day / 10, color);
  setDigit(2, datetime.Day % 10, color);
  setSeparator(SEPARATOR_DOT, color);
  setDigit(3, datetime.Month / 10, color);
  setDigit(4, datetime.Month % 10, color);
}

void displayBrightness() {
  CRGB color = CRGB(lightValue, lightValue, 0);
  setDigit(1, 10, color);
  setDigit(2, lightValue / 100, color);
  setSeparator(SEPARATOR_OFF, color);
  setDigit(3, lightValue % 100 / 10, color);
  setDigit(4, lightValue % 10, color);
}


void displayTemperature() {
  CRGB color = CRGB(lightValue, 0, 0);
  setDigit(1, roundf(temperature) / 10, color);
  setDigit(2, roundf(temperature * 10) / 10 % 10, color);
  //setSeparator(SEPARATOR_DOT, color);
  //setDigit(3, roundf(temperature * 10) % 10, color);
  //setDigit(4, 10, color);
  setSeparator(SEPARATOR_OFF, color);
  setDigit(3, 11, color);
  setDigit(4, 12, color);
}


void setDigit(byte pos, byte digit, CRGB color) {
  byte posStart = (pos - 1) * DIGIT_WIDTH * DIGIT_HEIGHT;
  byte posFinish = pos * DIGIT_WIDTH * DIGIT_HEIGHT;
  if (pos > 2) {
    // Skip separator vertical row
    posStart += DIGIT_HEIGHT;
    posFinish += DIGIT_HEIGHT;  
  }
  initStrip(digit, posStart, posFinish, color);
}


void setSeparator(byte state, CRGB color) {
  byte offset = 2 * DIGIT_WIDTH * DIGIT_HEIGHT;
  for (byte i = offset; i < offset + DIGIT_HEIGHT; i++) {
    if (separator7[state][i - offset] == 0) {
      strip[i] = CRGB::Black;
    } else {
      strip[i] = color;
    }
  }
}

void initStrip(byte digit, byte start, byte finish, CRGB color) {
  for (byte i = start; i < finish; i++) {
    if (digits47[digit][i - start] == 0) {
      strip[i] = CRGB::Black;
    } else {
      strip[i] = color;
    }
    // https://github.com/FastLED/FastLED/wiki/Pixel-reference
  }
}
